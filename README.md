&darr;

# TODO: look at https://github.com/tschumann/simplefamilytree for inspiration?

&uarr;

&uarr;

&uarr;




CoxPiano
========
_also known as **SimplePiano**_

[Doug Cox](http://jdmcox.com) wrote a free and open source utility for MIDI control that works with external controllers as well as Microsoft&rsquo;s MIDI synthesizer. His website states he has been programming since 1982. In the best case scenario, he&rsquo;s including his time copying code from RUN magazine (although he would&rsquo;ve been clairvoyant; their printing began in 1984) when he was six or ten. Without knowing Mr. Cox, I assume he is nearing or exceeds retirement age, so someone needs to come along and take the MIDI torch, because the current state of apps available through Github and the Microsoft Store seem low-quality, profit-oriented, and often both.

I changed the name from **SimplePiano** to **CoxPiano** both to honor the author and because that specific phrase is a SEO nightmare. I was nearly unable to find the original webpage until I recalled some of the more unusual terms on the original website. There are a couple of results for &ldquo;CoxPiano&rdquo;, but not enough to disrupt a search for a MIDI utility, and the name is more memorable after two decades than &ldquo;Mark&rsquo;s Standard Midi and Karaoke Song Player&rdquo;.

(By the way, if anyone wants to necro Mark Feaver's karaoke software&hellip; That was probably the best music teacher I could ask for in a rural town and on par with some of the better DAW software from today.)


## Build instructions

### Compile on a local machine

1. Clone **CoxPiano.c** locally
2. Open _x86_x64 Cross Tools Command Prompt for VS 2019_, or whichever 64-bit
3. Run something like `cl.exe CoxPiano.c winmm.lib gdi32.lib comctl32.lib user32.lib`

### Use a Gitlab runner

1. A runner capable of (and configured for) running the C++ command line tools for Visual Studio must be configured
2. Trigger the runner
3. (TODO) The output executable will be in artifacts, releases, or pushed directly to the repository


## Issues

[**_Report issues in Gitlab!_**](https://gitlab.com/PennRobotics/coxpiano/-/issues/new)


## Functionality

(text by original author)

_SimplePiano_ is an aid for anyone learning to play piano and read music.  
It shows the notes played on the treble and bass clef staffs.  
The notes played are also highlighted on a 60-key on-screen piano keyboard.  
You can play it using the keyboard, the mouse, or a MIDI keyboard.  
The Circle of Fifths can be displayed, and when a key in it is selected, that key&rsquo;s signature is shown on the staffs.  
Any of the eight most-used chords can be selected and played by playing the chord&rsquo;s root key. Chord inversions can also be selected.  
A random note reading exercise is included.  


## Changelog

### 1.18

* Build using [_Microsoft Visual Studio 2019_](https://visualstudio.microsoft.com/vs/older-downloads/)
* Target changed to 64-bit Intel-compatible. To support this change, some variable types were modified in the original source e.g. from UINT to UINT\_PTR, from GWL\_WNDPROC to GWLP_WNDPROC, and a few others. (This occurred before the initial source commit.)

